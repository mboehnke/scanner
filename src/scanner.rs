use std::{
    collections::HashMap,
    ffi::CString,
    path::{Path, PathBuf},
};

use apply::Apply;
use image::{DynamicImage, ImageBuffer, ImageFormat, Luma, Rgb};
use log::info;
use sane_scan::{Device, DeviceHandle, DeviceOption, DeviceOptionValue, Sane};

type Error = Box<dyn std::error::Error>;

pub struct Scanner {
    _sane: Sane,
    _device: Device,
    handle: DeviceHandle,
    vendor: String,
    model: String,
    name: String,
    options: HashMap<String, DeviceOption>,
    resolution: i32,
    depth: i32,
    color: bool,
    contrast: i32,
    out_dir: PathBuf,
}

impl Scanner {
    pub fn new(out_dir: &Path) -> Result<Self, Error> {
        let sane = sane_scan::Sane::init(1)?;
        let device = sane.get_devices()?.pop().ok_or("no devices found")?;
        let handle = device.open()?;
        let vendor = device.vendor.to_string_lossy().to_string();
        let model = device.model.to_string_lossy().to_string();
        let name = device.name.to_string_lossy().to_string();
        let options = handle
            .get_options()?
            .into_iter()
            .map(|o| (o.name.to_string_lossy().to_string(), o))
            .collect();

        Ok(Self {
            _sane: sane,
            _device: device,
            handle,
            vendor,
            model,
            name,
            options,
            resolution: 300,
            depth: 8,
            color: true,
            contrast: 0,
            out_dir: out_dir.to_path_buf(),
        })
    }

    pub fn name(&self) -> String {
        format!("{} {} ({})", self.vendor, self.model, self.name,)
    }

    pub fn scan(&mut self) -> Result<PathBuf, Error> {
        info!("scanning: {}", self.options());

        let set_option = |name, value| -> Result<(), Error> {
            let opt = self.get_option(name)?;
            self.handle.set_option(opt, value)?;
            Ok(())
        };

        let mode = if self.color { "Color" } else { "Gray" }.apply(CString::new)?;
        set_option("mode", DeviceOptionValue::String(mode))?;
        set_option("resolution", DeviceOptionValue::Int(self.resolution))?;
        set_option("depth", DeviceOptionValue::Int(self.depth))?;
        set_option("contrast", DeviceOptionValue::Int(self.contrast))?;

        let params = self.handle.start_scan()?;

        let buf = self.handle.read_to_vec()?;
        let width = params.pixels_per_line as u32;
        let height = params.lines as u32;
        let path = self
            .out_dir
            .join(chrono::Utc::now().format("%Y%m%d%H%M%S").to_string())
            .with_extension("png");

        info!("writing image to: {path:?}");

        if self.color {
            ImageBuffer::<Rgb<u8>, Vec<u8>>::from_vec(width, height, buf).map(DynamicImage::from)
        } else {
            ImageBuffer::<Luma<u8>, Vec<u8>>::from_vec(width, height, buf).map(DynamicImage::from)
        }
        .ok_or("could not parse image data")?
        .save_with_format(&path, ImageFormat::Png)?;

        Ok(path)
    }

    fn options(&self) -> String {
        format!(
            "resolution: {}dpi, depth: {}bit, mode: {}, contrast: {}",
            self.resolution,
            self.depth,
            if self.color { "color" } else { "gray" },
            self.contrast,
        )
    }

    fn get_option(&self, name: &str) -> Result<&DeviceOption, String> {
        self.options
            .get(name)
            .ok_or_else(|| format!("option {name:?} not found"))
    }

    pub fn button_press(&self) -> Result<Option<&'static str>, Error> {
        for name in ["file", "scan", "copy", "email"] {
            let opt = self.get_option(name)?;
            if let Ok(DeviceOptionValue::Bool(pressed)) = self.handle.get_option(opt) {
                if pressed {
                    return Ok(Some(name));
                }
            }
        }
        Ok(None)
    }

    pub fn settings(
        &mut self,
        resolution: i32,
        depth: i32,
        color: bool,
        contrast: i32,
    ) -> &mut Self {
        self.resolution = resolution;
        self.depth = depth;
        self.color = color;
        self.contrast = contrast;
        self
    }
}
