mod scanner;

use std::{path::PathBuf, time::Duration};

use apply::Apply;
use log::{error, info, LevelFilter};

use crate::scanner::Scanner;

type Error = Box<dyn std::error::Error>;

fn action(name: &str, scanner: &mut Scanner) -> Result<(), Error> {
    let (id, desc) = match name {
        "file" => (1, "[PDF]"),
        "scan" => (2, "[AUTO SCAN]"),
        "copy" => (3, "[COPY]"),
        "email" => (4, "[E-MAIL]"),
        _ => return Err(format!("button {name:?} unknown").into()),
    };
    info!("button pressed: {desc}");
    match id {
        1 => {
            scanner.settings(300, 8, false, 35).scan().map_err(|e| {
                error!("{e}");
                e
            })?;
        }
        4 => {
            scanner.settings(600, 8, true, 0).scan().map_err(|e| {
                error!("{e}");
                e
            })?;
        }
        2 | 3 => {}
        _ => unreachable!(),
    };
    Ok(())
}

fn main() -> Result<(), Error> {
    env_logger::Builder::new()
        .filter(None, LevelFilter::Info)
        .init();

    let out_dir = std::env::args()
        .nth(1)
        .unwrap_or_else(|| ".".into())
        .apply(PathBuf::from);

    let mut scanner = Scanner::new(&out_dir)?;

    info!("scanner detected: {}", scanner.name());

    loop {
        if let Some(button) = scanner.button_press()? {
            if let Err(e) = action(button, &mut scanner) {
                error!("{e}");
            }
        }
        std::thread::sleep(Duration::from_millis(100))
    }
}
